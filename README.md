# Uni-App JXC移动端库存管理前端项目

#### 介绍
针对移动端开发的仓库管理系统，无PC端，内部会员模式不支持注册，对货物进行增删改查并进行记录，拥有严格的资格验证，防止数据泄露，后期会支持条形码和二维码快速入库和查询

#### 软件架构
Uni-App开发，目前只针对Web端进行完整的兼容，其他各端未经验证


#### 安装教程

1.  克隆后直接用HBulder打开即可

#### 使用说明

1.  在[./const/server.js](https://gitee.com/JiameWork/jsc_web/blob/master/const/server.js)中修改mainServer为后端地址即可
