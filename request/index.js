import {
	mainServer
} from "@/const/server.js"
const reqLogin = (name, pwd) => {
	return uni.$request("login", "POST", {
		name,
		pwd
	})
}

const verifyUser = () => uni.$request("login_verify", "GET")

const getHouseList = (pageInterface) => uni.$request("articlelist", "POST", pageInterface)

const addHouse = (houseInterface) => uni.$request("article", "POST", houseInterface)

const getHouseDetial = (id) => uni.$request(`article?id=${id}`)
 
const getClassKeyHouse = (idBase) =>  uni.$request(`inventory?id=${idBase}`)

const getAddHouse = (data) => uni.$request(`inventory`,"POST",data)

const getDelHosue = (id) => uni.$request(`article?id=${id}`,"DELETE")

const getLogHouse = (pageInterface) => uni.$request("records","POST",pageInterface)

export {
	reqLogin,
	verifyUser,
	getHouseList,
	addHouse,
	getHouseDetial,
	getClassKeyHouse,
	getAddHouse,
	getDelHosue,
	getLogHouse
}
