const initToken = () => {
	uni.getStorage({
		key: "token",
		success(token) {
			uni.$token = token.data
		}
	})
}

initToken()

export {
	initToken
}
