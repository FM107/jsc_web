export const uploadImg = (url) => {
	return new Promise((reslove,reject)=>{
		uni.uploadFile({
			url: 'https://www.hualigs.cn/api/upload', // 仅为示例，非真实的接口地址
			filePath: url,
			name:"image",
			formData: {
				token: '67bd78681b9dedb27ac902a9091f4bb7',
				apiType:"ali,huluxia"
			},
			success: ({data}) => {
				data = JSON.parse(data)
				reslove(data.data.url.distribute)
			},
			file(err){
				reject(err)
			}
		});
	})
}