export const neatFigure = (value) => {
	const valueType = typeof value
	if (valueType === "string" || valueType === "number") {
		if (valueType === "string") {
			value = Number(valueType)
			if (isNaN(value)) {
				return 0
			}
		}
		if (value > 100000000) {
			value = Math.floor(value / 100000000 * 100) / 100
			return `${value}亿`
		} else if (value > 10000000) {
			value = Math.floor(value / 10000000 * 100) / 100
			return `${value}千万`
		} else if (value > 10000) {
			value = Math.floor(value / 10000 * 100) / 100
			return `${value}万`
		} else if (value > 1000) {

			value = Math.floor(value / 1000 * 100) / 100
			return `${value}千`
		} else {
			return value
		}
	} else {
		return 0
	}
}
