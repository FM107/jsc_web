import { mainServer } from '@/const/server.js'
export default (url,methods = "GET",data) => {
	const token = uni.$token
	const allUrl = `${mainServer}/${url}`
		return new Promise((reslove,reject)=>{
		uni.request({
			url:allUrl,
			method:methods,
			data:data,
			header:{
				token
			},
			success(res) {
				if (res.statusCode !== 200) {
					console.log(res.data)
					if(res.data.message === 'error token'){
						uni.showToast({
							title: "登录失效,重新登录",
							icon: "close",
							success() {
								setTimeout(()=>{
									uni.navigateTo({
										url:"/pages/login/login"
									})
								},1000)
							}
						})
					}
					reject("request error: " + res.errMsg)
				}else{
					if (res.data.status) {
						reslove(res.data.data)
					}else{
						reject(res.data.message)
					}
				}
			},
			fail(err) {
				reject(err)
			}
		})
	})
}