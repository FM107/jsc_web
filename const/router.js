const autorityLoginRoutes = [
	"/pages/index/index",
	"/pages/addHouse/addHouse",
	"/pages/users/users",
	"/pages/initTheInventory/initTheInventory"
]

export {
	autorityLoginRoutes
}
