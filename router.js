import {
	RouterMount,
	createRouter
} from 'uni-simple-router';
import {
	autorityLoginRoutes
} from "@/const/router.js"

import {
	verifyUser
}
from "@/request/index.js"

const router = createRouter({
	platform: process.env.VUE_APP_PLATFORM,
	routes: [...ROUTES]
});
//全局路由前置守卫
router.beforeEach(async (to, from, next) => {
	if (autorityLoginRoutes.includes(to.path)) {
		try {
			await verifyUser()
			next()
		} catch (e) {
			next(false)
		}
	} else {
		next();
	}
});
export {
	router,
	RouterMount
}
