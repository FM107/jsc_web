import Vue from 'vue'
import App from './App'
import {
	router,
	RouterMount
} from './router.js'
import uView from './uni_modules/uview-ui'
import request from "@/tools/ajaxPromise.js"
import '@/tools/token.js'
uni.$request = request
Vue.use(router)
Vue.use(uView)
App.mpType = 'app'
const app = new Vue({
	...App
})

// #ifdef H5
RouterMount(app, router, '#app') //兼容h5
// #endif

// #ifndef H5
app.$mount();
// #endif
